$("div.mobile-nav-button").click ->
 $("nav#nav, nav#nav-2").toggle()

$("div.mobile-nav-button p").click ->
 $("div.mobile-nav-button i").toggleClass("fa-bars fa-times")

$("nav#nav > ul > li > a").click ->
 $(this).next('div.sub-nav-container').toggle(0)
 $(this).toggleClass("down-arrow active")

$("nav#nav ul li li:first-child").click ->
 $(this).nextAll('div.sub-nav-container div li').toggle(0)

$("#nav-2-container #nav-2 ul li a").click ->
 $(this).next('#nav-2 ul li ul').toggle(0)
 $(this).toggleClass("down-arrow active")