(function() {
  $("div.mobile-nav-button").click(function() {
    return $("nav#nav, nav#nav-2").toggle();
  });

  $("div.mobile-nav-button p").click(function() {
    return $("div.mobile-nav-button i").toggleClass("fa-bars fa-times");
  });

  $("nav#nav > ul > li > a").click(function() {
    $(this).next('div.sub-nav-container').toggle(0);
    return $(this).toggleClass("down-arrow active");
  });

  $("nav#nav ul li li:first-child").click(function() {
    return $(this).nextAll('div.sub-nav-container div li').toggle(0);
  });

  $("#nav-2-container #nav-2 ul li a").click(function() {
    $(this).next('#nav-2 ul li ul').toggle(0);
    return $(this).toggleClass("down-arrow active");
  });

}).call(this);
